# Changelog

<!-- 
added: New feature
fixed: Bug fix
changed: Feature change
deprecated: New deprecation
removed: Feature removal
security: Security fix
performance: Performance improvement
other: Other 
-->

## [Unreleased]
- Sorry for my English, I use a translator. :)

## [v1] - 2022-02-03
### Added
- Initial release.

## [v2] - 2022-02-04
### Fixed
- Change GLib.idle_add() to 'realize' signal in buildPrefsWidget (prefs.js).
- Add settings-schema and gettext-domain in metadata.json.
- Remove unnecessary imports.
- Fix indentations.
- Elimination of unnecessary logs.
- Thanks JustPerfection!!!

## [v3] - 2022-02-05
### Added
- Add alernatives thresholds paths.
- Add available flag and the _init code is restructured according to this addition... Now the submenu is shown even if the thresholds are not available.
### Fixed
- Fix _available function. The test whether the files exist points to the start file twice!

## [v4] - 2022-02-21
### Added
- Add icon type option (symbolic/color).

## [v5] - 2022-02-21
### Fixed
- Fix icon type on start.

## [v6] - 2022-03-16
### Added
- Threshold related functions have been moved to a library and optimized.
- The option to apply the thresholds to the battery of the dock is implemented if it is available (try this, I don't have a dock!!!)
- Added option to show/hide current values in menu.
- Added tooltips in menu icons.
### Changed
- The settings are applied immediately except for the thresholds that will be applied the next time they are activated from this extension or using the button for this purpose in the preferences window.
- Remove thresholds limits.
### Performance
- Function to apply thresholds now use promise (async) and can catch errors.
- Function to apply thresholds now checks if are available to determine what can be applied (more compatibility?).
### Fixed
### Other
- Reformat strings.
- Icons are redesigned... something.
- Minor cosmetic changes. 

## [v7] - 2022-03-21
### Fixed
- If the action of applying the thresholds is canceled, now it does not throw an error.
- Update error notification message.
- Fixed the problem that the state switch was not updated to the correct state in case of an error.
- Fixed texts.
### Other
- Change schema settings types.
- Preferences UI was moved to a .ui file and Gtk4 is implemented with cosmetic improvements.
- Files names changed.
### Changed
- The buttons to apply the thresholds on the settings page are removed... I didn't like it :)
- Validate availability before applying the threshold in extension.js.
- Update metadata information.
### Performance
- Improvements to file monitors to avoid repeated execution of the callback.
- Change GtkComboBoxText to GtkDroopDown.
### Added
- Added an option to show/hide tooltips.
- Gnome 42 and libadwaita compatibility.

## [v8] - 2022-03-27
### Performance
- Threshold write performance improvements (fewer writes). Inspired by [TLP](https://github.com/linrunner/TLP/blob/main/bat.d/05-thinkpad)
- Russian translation update (Andrey Sitnik)

## [v9] - 2022-03-28
### Fixed
- Russian translation update (Andrey Sitnik)

## [v10] - 2022-05-25
### Added
- Gnome 42 icons
- Added a menu to apply the configured values
- Added option to disable notifications
### Changed
- Revert switch to menu
### Other
- Update icons

## [v11] - 2022-05-30
### Fixed
- Russian translation update (Andrey Sitnik)

## [v12] - 2022-10-08
### Added
- Add option to show icon on inactive thresholds (based on the proposal of [Riccardo Massidda](https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/issues/3))
- Gnome 43 compatibility (QuickSettings) - First attempt... many TODOs
### Fixed
- Dock battery callbacks

## [v13] - 2022-10-16
### Fixed
- Issue [#5](https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/issues/5): Bad function name
- Warnings fixes

## [v14] - 2022-11-28
### Performance
- Completely rewrites the "driver" using (or trying to) the GObject model
- Part of the "indicator" is rewritten
- Some text strings were modified to adapt them (The driver does not show text)
### Changed
- Thresholds can now be adjusted with 1% intervals
### Removed
- Support for versions older than Gnome 43 is removed. (Older versions of the extension are functional on Gnome 41/42)

## [v15] - 2022-12-01
### Fixed
- Fix platform detection (wrong regular expression)

## [v16] - 2022-12-23
### Added
- Added the option to reset the recommended thresholds on the preferences page
- Added the option to reset all preferences
### Other
- Move preference pages to separate classes
- Moved links to preferences window menu. Based on the excellent <b>[GNOME Shell Extension - Blur my Shell](https://github.com/aunetx/blur-my-shell)</b>: All credits to [Aurélien Hamy](https://github.com/aunetx)

## [v17] - 2022-12-27
### Other
- Update translations

## [v18] - 2023-01-03
### Other
- Update translations

## [v19] - 2023-01-24
### Fixed
- Issue [#8](https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/issues/8): On some models the start threshold 0 is not allowed, instead 95 is used (Ex: E14 Gen 3)
### Other
- Update translations

## [v20] - 2023-01-27
### Other
- Update translations