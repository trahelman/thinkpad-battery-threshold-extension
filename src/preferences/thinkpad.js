'use strict'

const { Adw, GLib, GObject, Gio } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

var Thinkpad = GObject.registerClass({
    GTypeName: 'ThinkpadPrefs',
    Template: `file://${GLib.build_filenamev([Me.path, 'ui', 'thinkpad.ui'])}`,
    InternalChildren: [
        'start_bat0',
        'end_bat0',
        'start_bat1',
        'end_bat1',
        'reset'
    ],
}, class Thinkpad extends Adw.PreferencesPage {
    constructor(settings) {
        super({});
        
        settings.bind(
            'start-bat0', 
            this._start_bat0, 
            'value', 
            Gio.SettingsBindFlags.DEFAULT
        );
        settings.bind(
            'end-bat0', 
            this._end_bat0, 
            'value', 
            Gio.SettingsBindFlags.DEFAULT
        );
        settings.bind(
            'start-bat1', 
            this._start_bat1, 
            'value', 
            Gio.SettingsBindFlags.DEFAULT
        );
        settings.bind(
            'end-bat1', 
            this._end_bat1, 
            'value', 
            Gio.SettingsBindFlags.DEFAULT
        );

        settings.connect('changed::start-bat0', () => {
            if (this._start_bat0.value >= this._end_bat0.value) {
                this._end_bat0.value = this._start_bat0.value + 1;
            }
        });
        settings.connect('changed::end-bat0', () => {
            if (this._start_bat0.value >= this._end_bat0.value) {
                this._start_bat0.value = this._end_bat0.value - 1;
            }
        });
        settings.connect('changed::start-bat1', () => {
            if (this._start_bat1.value >= this._end_bat1.value) {
                this._end_bat1.value = this._start_bat1.value + 1;
            }
        });
        settings.connect('changed::end-bat1', () => {
            if (this._start_bat1.value >= this._end_bat1.value) {
                this._start_bat1.value = this._end_bat1.value - 1;
            }
        });

        this._reset.connect('clicked', () => {
            const keys = [
                'start-bat0',
                'end-bat0',
                'start-bat1',
                'end-bat1'
            ];
            keys.forEach(key => {
                settings.reset(key);
            });
        });
    }
});