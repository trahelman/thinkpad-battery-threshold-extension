'use strict'

const { Adw, GLib, GObject, Gio } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


var Experimental = GObject.registerClass({
    GTypeName: 'ExperimentalPrefs',
    Template: `file://${GLib.build_filenamev([Me.path, 'ui', 'experimental.ui'])}`,
    InternalChildren: [
        'debug_mode',
        'model_verification',
        'reset_all'
    ],
}, class Experimental extends Adw.PreferencesPage {
    constructor(settings) {
        super({});

        settings.bind(
            'debug-mode', 
            this._debug_mode, 
            'state', 
            Gio.SettingsBindFlags.DEFAULT
        );
        settings.bind(
            'model-verification', 
            this._model_verification, 
            'state', 
            Gio.SettingsBindFlags.DEFAULT
        );

        this._reset_all.connect('clicked', () => {
            this._resetSettings(settings);
        });
    }

    /**
     * Reset all (recursively) settings values to default
     * 
     * @param {Gio.Settings} settings Settings to reset
     */
    _resetSettings(settings) {
        const keys = settings.settings_schema.list_keys();
        keys.forEach(key => {
            settings.reset(key);
        });

        const childrens = settings.settings_schema.list_children();
        childrens.forEach(children => {
            const childrenSettings = settings.get_child(children);
            this._resetSettings(childrenSettings);
        });
    }
});