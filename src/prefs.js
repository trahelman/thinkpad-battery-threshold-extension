'use strict';

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const { addMenu } = Me.imports.preferences.menu;
const { General } = Me.imports.preferences.general;
const { Thinkpad } = Me.imports.preferences.thinkpad;
const { Experimental } = Me.imports.preferences.experimental;


/**
 * prefs initiation
 *
 * @returns {void}
 */
function init() {
    ExtensionUtils.initTranslations(Me.metadata.uuid);
}

/**
 * fill prefs window
 *
 * @returns {void}
 */
function fillPreferencesWindow(window) {

    const settings = ExtensionUtils.getSettings();

    addMenu(window);

    window.add(new General(settings));
    window.add(new Thinkpad(settings));
    window.add(new Experimental(settings));

    window.search_enabled = true;
}