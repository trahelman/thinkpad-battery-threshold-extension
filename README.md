# Thinkpad Battery Threshold Extension

<b>Gnome extension for Enable/Disable battery threshold on Lenovo ThinkPad laptops</b>

If you mainly use the system with the AC power adapter connected and only use the battery sporadically, you can increase battery life by setting the maximum charge value to less than 100%. This is useful because batteries that are used sporadically have a longer lifespan when kept at less than full charge.

Compatible with Lenovo ThinkPad series from model year 2011 (theoretically).

To check if the function is available, see if the files <b>charge_control_start_threshold</b> and <b>charge_control_end_threshold</b> (valid alternatives: <b>charge_start_threshold</b> and <b>charge_stop_threshold</b>) exist in the <b>/sys/class/power_supply/BAT[0-1]</b> directory, also you must have permissions to read and write (in case you do not have write permissions, the root password will be requested to modify the values).

## Install
1. Open [Thinkpad Battery Threshold] on GNOME Shell Extensions site.
2. Click slider to install extension.

[Thinkpad Battery Threshold]: https://extensions.gnome.org/extension/4798/thinkpad-battery-threshold/

## Debug
- For more information, enable <b>Debug mode</b> in the extension settings and run the following command in a terminal to get the messages:

`journalctl -f -o cat GNOME_SHELL_EXTENSION_UUID=thinkpad-battery-threshold@marcosdalvarez.org`

- You can try disabling <b>Force compatible models</b> to test if it works on your platform.

## Translations
You can help us translating extension to your language.

[Translate on Weblate](https://hosted.weblate.org/engage/thinkpad-battery-threshold/)

[![Estado de la traducción](https://hosted.weblate.org/widgets/thinkpad-battery-threshold/-/horizontal-auto.svg)](https://hosted.weblate.org/engage/thinkpad-battery-threshold/)

## Screenshots
![Gnome 43](/images/Gnome%2043.png)*Gnome 43*

![Gnome 42](/images/Gnome%2042.png)*Gnome 42*

![Gnome 41](/images/Gnome%2041.png)*Gnome 41*
